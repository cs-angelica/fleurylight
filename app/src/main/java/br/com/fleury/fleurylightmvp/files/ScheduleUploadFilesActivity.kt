package br.com.fleury.fleurylightmvp.files

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import br.com.fleury.fleurylightmvp.R
import kotlinx.android.synthetic.main.activity_schedule_upload_files.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.ExifInterface
import android.support.v4.app.NavUtils
import android.view.MenuItem
import br.com.fleury.fleurylightmvp.ConclusionActivity


class ScheduleUploadFilesActivity: AppCompatActivity() {

    private var currentPhotoPath : String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_upload_files)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        addReceiptButton.setOnClickListener {
            //            dispatchTakePictureIntent(REQUEST_IMAGE_ORDER)
            addFileNameToContainer(receiptContainer, "")
        }
        addHealthPlanButton.setOnClickListener {
            //            dispatchTakePictureIntent(REQUEST_IMAGE_ASSURANCE)
            addFileNameToContainer(healthPlanContainer, "")
        }
        buttonContinue.setOnClickListener { startActivity(Intent(this, ConclusionActivity::class.java)) }
    }

    private fun dispatchTakePictureIntent(requestCode: Int) {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (takePictureIntent.resolveActivity(this.packageManager) != null) {

            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // TODO put error treatment code
            }

            if (photoFile != null) {
                val photoURI : Uri = FileProvider.getUriForFile(this,
                        "br.com.fleury.fleurylightmvp.fileprovider",
                        photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, requestCode)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK) {
            val file = File(currentPhotoPath)
            if(file.exists()) {
                if(requestCode == REQUEST_IMAGE_ORDER) {
                    addFileNameToContainer(receiptContainer, file.name)
                } else {
                    addFileNameToContainer(healthPlanContainer, file.name)
                }
            }
        }
    }

    private fun addFileNameToContainer(container: ViewGroup, fileName: String) {
        val fileItem: View = LayoutInflater.from(this).inflate(R.layout.file_item, null)
        val fileItemText: TextView = fileItem.findViewById(R.id.fileName)
        val fileLabel: TextView = fileItem.findViewById(R.id.fileLabel)
        val fileThumbnail: ImageView = fileItem.findViewById(R.id.fileThumbnail)
        val removeFileButton: ImageButton = fileItem.findViewById(R.id.removeFileButton)

        fileItemText.text = fileName
        fileLabel.text = getString(R.string.picture_number, container.childCount)

        setPicture(fileThumbnail)

        removeFileButton.setOnClickListener {
            container.removeView(fileItem)
            if(container.childCount == 1) {
                container.visibility = View.GONE
            }
        }

        container.addView(fileItem)
        container.visibility = View.VISIBLE
    }

    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if(storageDir != null) {
            val image = File.createTempFile(
                    imageFileName, /* prefix */
                    ".jpg", /* suffix */
                    storageDir      /* directory */
            )

            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = image.absolutePath
            return image
        }
        return null
    }

    private fun setPicture(imageView: ImageView) {

        if(currentPhotoPath != null) {
            // Get the dimensions of the View
            val targetW = imageView.maxWidth
            val targetH = imageView.maxHeight

            // Get the dimensions of the bitmap
            val bmOptions = BitmapFactory.Options()
            bmOptions.inJustDecodeBounds = true
            BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
            val photoW = bmOptions.outWidth
            val photoH = bmOptions.outHeight

            // Determine how much to scale down the image
            val scaleFactor = Math.min(photoW / targetW, photoH / targetH)

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false
            bmOptions.inSampleSize = scaleFactor
            bmOptions.inPurgeable = true

            val bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
            imageView.setImageBitmap(getBitmapWithRotation(bitmap))
        } else {
            imageView.setImageResource(R.drawable.ic_image)
        }

    }

    fun getBitmapWithRotation(bitmap: Bitmap) : Bitmap {
        val ei = ExifInterface(currentPhotoPath);
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED)

        var rotatedBitmap: Bitmap? = null
        when(orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotatedBitmap = rotateImage(bitmap, 90)

            ExifInterface.ORIENTATION_ROTATE_180 -> rotatedBitmap = rotateImage(bitmap, 180)

            ExifInterface.ORIENTATION_ROTATE_270 -> rotatedBitmap = rotateImage(bitmap, 270);

            ExifInterface.ORIENTATION_NORMAL -> rotatedBitmap = bitmap

            else -> rotatedBitmap = bitmap
        }

        return rotatedBitmap
    }

    fun rotateImage(source: Bitmap, angle: Int): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle.toFloat())
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height,
                matrix, true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private companion object {
        val REQUEST_IMAGE_ORDER = 1
        val REQUEST_IMAGE_ASSURANCE = 2
    }
}