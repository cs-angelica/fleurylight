package br.com.fleury.fleurylightmvp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_schedules.*
import kotlinx.android.synthetic.main.item_schedule.view.*

class SchedulesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedules)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        startItem1()
        startItem2()
        startItem3()
        startItem4()
        startItem5()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun startItem5() {
        item5.textExamName.text = "Exame de urina - Tipo 1"
        item5.textScheduledStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                R.drawable.ic_cancel, 0, 0, 0)
        item5.textScheduledStatus.text = "Cancelado"
        item5.buttonDetail.text = "Remarcar"
        item5.buttonDetail.setOnClickListener {
            // TODO mudar para activity de data
            //startActivity(Intent(this, ScheduleExamActivity::class.java))
        }
    }

    private fun startItem4() {
        item4.textExamName.text = "Exame de sangue - Colesterol"
        item4.textScheduledStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                R.drawable.ic_cancel, 0, 0, 0)
        item4.textScheduledStatus.text = "Cancelado"
        item4.buttonDetail.text = "Remarcar"
        item4.buttonDetail.setOnClickListener {
            // TODO mudar para activity de data
//            startActivity(Intent(this, ScheduleExamActivity::class.java))
        }
    }

    private fun startItem3() {
        item3.textExamName.text = "Ecocardiograma"
        item3.textScheduledStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                R.drawable.ic_cancel, 0, 0, 0)
        item3.textScheduledStatus.text = "Cancelado"
        item3.buttonDetail.text = "Remarcar"
        item3.buttonDetail.setOnClickListener {
            // TODO mudar para activity de data
//            startActivity(Intent(this, ScheduleExamActivity::class.java))
        }
    }

    private fun startItem2() {
        item2.textExamName.text = "Polissonografia"
        item2.textScheduledStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                R.drawable.ic_check, 0, 0, 0)
        item2.textScheduledStatus.text = "Realizado em 28 de julho de 2018 às 14:00"
        item2.buttonDetail.text = "Resultados"
        item2.buttonDetail.setOnClickListener {
            AlertDialog.Builder(this)
                    .setTitle("Resultado de exames")
                    .setMessage("O resultado ainda não está disponível.")
                    .setCancelable(true)
                    .setPositiveButton("OK", { dialog, _ -> dialog.dismiss() })
                    .create()
                    .show()
        }
    }

    private fun startItem1() {
        if (intent.hasExtra(NAME_OF_EXAM)) {
            item1.textExamName.text = intent.getStringExtra(NAME_OF_EXAM)
        } else {
            item1.textExamName.text = "Exame de sangue - Colesterol"
        }

        item1.textScheduledStatus.text = "Agendado para Amanhã às 11:00"
        item1.textScheduledStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                R.drawable.ic_calendar, 0, 0, 0)
        item1.buttonDetail.text = "Detalhes"
        item1.buttonDetail.setOnClickListener {
            startActivity(MyScheduleActivity.newIntent(this,
                                            item1.textExamName.text.toString(),
                                            item1.textScheduledStatus.text.toString()))
        }
    }

    companion object {

        val NAME_OF_EXAM = "NAME_OF_EXAM"

        fun newIntent(context: Context, examName: String): Intent {
            val intent = Intent(context, SchedulesActivity::class.java)
            intent.putExtra(NAME_OF_EXAM, examName)

            return intent
        }

    }
}
