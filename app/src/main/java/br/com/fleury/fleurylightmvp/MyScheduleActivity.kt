package br.com.fleury.fleurylightmvp

import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.view.MenuItem
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Button
import kotlinx.android.synthetic.main.activity_my_schedule.*

class MyScheduleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_schedule)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initLayout()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initLayout() {
        val examName: String = intent.extras[NAME_OF_EXAM] as String
        val examStatus: String = intent.extras[STATUS_OF_EXAM] as String

        textInfoExamName.text = examName
        textInfoScheduledStatus.text = examStatus

        initNurseInfo()

        initDeliverDateCard()

        initInstructionsCard()

        initOthersCard()
    }

    private fun initNurseInfo() {

        buttonCall.setOnClickListener {
            AlertDialog.Builder(this)
                    .setMessage("O telefone do enfermeiro não está disponivel no momento")
                    .setCancelable(true)
                    .setPositiveButton("OK", { dialog, _ -> dialog.dismiss() })
                    .create()
                    .show()
        }

        buttonChat.setOnClickListener {
            AlertDialog.Builder(this)
                    .setMessage("Não foi possível entrar em contato com o enfermeiro via Whatsapp")
                    .setCancelable(true)
                    .setPositiveButton("OK", { dialog, _ -> dialog.dismiss() })
                    .create()
                    .show()
        }
    }

    private fun initOthersCard() {
        cardOthers.viewTreeObserver.addOnPreDrawListener(
                object : ViewTreeObserver.OnPreDrawListener {

                    override fun onPreDraw(): Boolean {
                        cardOthers.viewTreeObserver.removeOnPreDrawListener(this)
                        val layoutParams = cardOthers.layoutParams

                        layoutParams.height = cardOthers.minimumHeight
                        cardOthers.layoutParams = layoutParams

                        return true
                    }
                })
        buttonOthers.setOnClickListener {
            toggleCardViewnHeight(cardDeliverDate.minimumHeight + 50, cardOthers)
        }
    }

    private fun initInstructionsCard() {
        cardInstructions.viewTreeObserver.addOnPreDrawListener(
                object : ViewTreeObserver.OnPreDrawListener {

                    override fun onPreDraw(): Boolean {
                        cardInstructions.viewTreeObserver.removeOnPreDrawListener(this)
                        val layoutParams = cardInstructions.layoutParams

                        layoutParams.height = cardInstructions.minimumHeight
                        cardInstructions.layoutParams = layoutParams

                        return true
                    }
                })
        buttonInstructions.setOnClickListener {
            toggleCardViewnHeight(cardDeliverDate.minimumHeight * 5 + 100, cardInstructions)
        }
    }

    private fun initDeliverDateCard() {
        cardDeliverDate.viewTreeObserver.addOnPreDrawListener(
                object : ViewTreeObserver.OnPreDrawListener {

                    override fun onPreDraw(): Boolean {
                        cardDeliverDate.viewTreeObserver.removeOnPreDrawListener(this)
                        val layoutParams = cardDeliverDate.layoutParams

                        layoutParams.height = cardDeliverDate.minimumHeight
                        cardDeliverDate.layoutParams = layoutParams

                        return true
                    }
                })
        buttonDeliverDate.setOnClickListener {
            toggleCardViewnHeight(cardDeliverDate.minimumHeight - 60, cardDeliverDate)
        }
    }

    private fun toggleCardViewnHeight(height: Int, cardView: CardView) {

        if (cardView.height == cardView.minimumHeight) {
            expandView(height, cardView)
        } else {
            collapseView(cardView)
        }
    }

    private fun collapseView(cardView: CardView) {

        val anim = ValueAnimator.ofInt(cardView.measuredHeightAndState, cardView.minimumHeight)

        anim.duration = 200
        anim.interpolator = AccelerateDecelerateInterpolator()

        anim.addUpdateListener { valueAnimator ->
            val animator = valueAnimator.animatedValue as Int
            val layoutParams = cardView.layoutParams

            layoutParams.height = animator
            cardView.layoutParams = layoutParams
        }
        anim.start()

        ((cardView.getChildAt(0) as ViewGroup).getChildAt(0) as Button)
                .setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand, 0)
    }

    private fun expandView(height: Int, cardView: CardView) {
        val anim = ValueAnimator.ofInt(cardView.measuredHeight, 60 + height + (height * 2.0).toInt())

        anim.duration = 200
        anim.interpolator = AccelerateDecelerateInterpolator()

        anim.addUpdateListener { valueAnimator ->
            val animator = valueAnimator.animatedValue as Int
            val layoutParams = cardView.layoutParams

            layoutParams.height = animator
            cardView.layoutParams = layoutParams
        }
        anim.start()

        ((cardView.getChildAt(0) as ViewGroup).getChildAt(0) as Button)
                .setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_down, 0)
    }

    companion object {

        const val NAME_OF_EXAM = "NAME_OF_EXAM"
        const val STATUS_OF_EXAM = "STATUS_OF_EXAM"

        fun newIntent(context: Context, examName: String, examStatus: String): Intent {
            val intent = Intent(context, MyScheduleActivity::class.java)
            intent.putExtra(NAME_OF_EXAM, examName)
            intent.putExtra(STATUS_OF_EXAM, examStatus)

            return intent
        }
    }
}
