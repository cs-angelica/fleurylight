package br.com.fleury.fleurylightmvp.search

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import br.com.fleury.fleurylightmvp.R
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.WindowManager
import br.com.fleury.fleurylightmvp.address.ScheduleAddressActivity
import br.com.fleury.fleurylightmvp.util.JsonUtil
import kotlinx.android.synthetic.main.activity_search_exam.*


class SearchExamActivity: AppCompatActivity() {
    private var examList: ArrayList<Exam> = arrayListOf()
    private lateinit var adapter: ExamListAdapter

    private val searchTextWatcher = object : TextWatcher {
        val filtered: ArrayList<Exam> = arrayListOf()
        override fun afterTextChanged(p0: Editable?) {

        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            filtered.clear()
            for (exam in examList) {
                if (s.isEmpty() || exam.name.toLowerCase().contains(s.toString().toLowerCase())) {
                    filtered.add(exam)
                }
            }

            adapter.changeItems(filtered)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_search_exam)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initLayout()
    }

    private fun initLayout() {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        // Exam list
        (JsonUtil.getDataFromJsonFile(this, "exam.json", Array<Exam>::class.java) as Array<Exam>).toCollection(examList)
        adapter = ExamListAdapter(this, examList)
        examsList.adapter = adapter

        textSearch.addTextChangedListener(searchTextWatcher)

        updateSelectedItemsLabel(0)
    }

    fun updateSelectedItemsLabel(amountSelected: Int) {
        selectedExamsLabel.text = String.format(resources.getString(R.string.count_selected_exams), amountSelected)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.options_menu, menu)

        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == R.id.done) {
            startActivity(Intent(this, ScheduleAddressActivity::class.java))

            getFirstSelectedExam()?.let {
                saveExamSharedPrefs(it)
            }

            return true
        } else if(item?.itemId == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun saveExamSharedPrefs(exam: Exam) {
        val sharedPref = applicationContext.getSharedPreferences("br.com.fleury.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(getString(R.string.exam_name_pref), exam.name)
                    .apply()
        }
    }

    private fun getFirstSelectedExam() :Exam?{
        for(exam: Exam in examList) {
            if(exam.isSelected)
                return exam
        }

        return null
    }
}