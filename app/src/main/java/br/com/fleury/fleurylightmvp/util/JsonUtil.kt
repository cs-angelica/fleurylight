package br.com.fleury.fleurylightmvp.util

import android.content.Context
import br.com.fleury.fleurylightmvp.search.Exam
import com.google.gson.Gson
import java.io.IOException
import java.nio.charset.Charset

object JsonUtil {

    private fun loadJSONFromAsset(context: Context, fileName: String): String? {
        var json: String? = null
        try {
            val inputStream = context.getAssets().open(fileName)

            val size = inputStream.available()

            val buffer = ByteArray(size)

            inputStream.read(buffer)

            inputStream.close()

            json = String(buffer, Charset.forName("UTF-8"))
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }


    fun getDataFromJsonFile(context: Context, fileName: String, clazz: Class<Array<Exam>>) : Any {
        val gson = Gson()

        val strJson = loadJSONFromAsset(context, fileName)

        return gson.fromJson(strJson, clazz)
    }

}