package br.com.fleury.fleurylightmvp.calendar

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioButton
import android.widget.RadioGroup
import br.com.fleury.fleurylightmvp.R
import br.com.fleury.fleurylightmvp.files.ScheduleUploadFilesActivity
import kotlinx.android.synthetic.main.activity_calendar.*

class CalendarActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (p0 != null) {
            if (p0.id == R.id.spinnerMonth && p2 == 0) {
                spinnerDays.isEnabled = false
                spinnerDays.setSelection(0)
                timers.clearCheck()

                textInfoSelectTimer.visibility = View.GONE
                timers.visibility = View.GONE
            } else if (p0.id == R.id.spinnerMonth && p2 != 0) {
                spinnerDays.isEnabled = true
                spinnerDays.setSelection(0)
                timers.clearCheck()

                textInfoSelectTimer.visibility = View.GONE
                timers.visibility = View.GONE
            } else if (p0.id == R.id.spinnerDays && p2 != 0) {
                textInfoSelectTimer.visibility = View.VISIBLE
                timers.visibility = View.VISIBLE
                timers.clearCheck()
            }
        }
    }

    private fun saveExamSharedPrefs(exam: String) {
        val sharedPref = applicationContext.getSharedPreferences("br.com.fleury.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(getString(R.string.exam_date_pref), exam)
                    .apply()
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        // do nothing
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val adapterMonths = ArrayAdapter.createFromResource(this, R.array.months_array, android.R.layout.simple_spinner_item)

        adapterMonths.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerMonth.adapter = adapterMonths
        spinnerMonth.onItemSelectedListener = this

        val adapterDays = ArrayAdapter.createFromResource(this, R.array.available_days_array, android.R.layout.simple_spinner_item)

        adapterDays.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDays.adapter = adapterDays
        spinnerDays.onItemSelectedListener = this

        spinnerDays.isEnabled = false

        timers.setOnCheckedChangeListener { radioGroup: RadioGroup, _: Int ->
            buttonContinue.isEnabled = false

            if (radioGroup.checkedRadioButtonId != -1) {
                buttonContinue.isEnabled = true
            }
        }

        buttonContinue.setOnClickListener {
            val selectedMonth = spinnerMonth.selectedItem.toString()
            val selectedDay = spinnerDays.selectedItem.toString()
            val selectedHour = timers.findViewById<RadioButton>(timers.checkedRadioButtonId).text

            saveExamSharedPrefs("$selectedDay de $selectedMonth de 2018 às $selectedHour")

            startActivity(Intent(this, ScheduleUploadFilesActivity::class.java))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
