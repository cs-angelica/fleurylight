package br.com.fleury.fleurylightmvp.address

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import br.com.fleury.fleurylightmvp.R
import br.com.fleury.fleurylightmvp.calendar.CalendarActivity
import kotlinx.android.synthetic.main.activity_schedule_address.*


class ScheduleAddressActivity : AppCompatActivity() {

    private var firstStepDone = false

    private val editTextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(charSequence: CharSequence?, start: Int, before: Int, count: Int) {
            if (textCep.text.isEmpty()) {
                buttonClearCep.visibility = View.INVISIBLE
            } else {
                buttonClearCep.visibility = View.VISIBLE
            }
        }
    }

    private val editorAction = TextView.OnEditorActionListener { _, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_NEXT) {
            searchAddressByCep()
            return@OnEditorActionListener true
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_address)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        textCep.addTextChangedListener(editTextWatcher)
        textCep.setOnEditorActionListener(editorAction)

        buttonClearCep.setOnClickListener {
            textCep.text.clear()
            firstStepDone = false
            addressContainer.visibility = View.INVISIBLE
        }
        buttonContinue.setOnClickListener {
            if (firstStepDone) {
                startActivity(Intent(this, CalendarActivity::class.java))
            } else {
                searchAddressByCep()
            }
        }
    }

    private fun searchAddressByCep() {
        if (textCep.text.isEmpty().not()) {

            firstStepDone = true
            addressContainer.visibility = View.VISIBLE
            textStreet.setText(getString(R.string.default_street))

            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(textCep.windowToken, 0)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


}
