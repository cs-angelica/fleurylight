package br.com.fleury.fleurylightmvp.search

import com.google.gson.annotations.SerializedName

data class Exam(val name: String, @SerializedName("mobile_available")val mobileAvailable: Boolean) {
    var isSelected: Boolean = false
}