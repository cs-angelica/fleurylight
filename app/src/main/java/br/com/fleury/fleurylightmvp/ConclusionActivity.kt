package br.com.fleury.fleurylightmvp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_conclusion.*

class ConclusionActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_conclusion)

        buttonHome.setOnClickListener { openHomeScreen() }
    }

    override fun onBackPressed() {
        openHomeScreen()
    }

    private fun openHomeScreen(){
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }
}