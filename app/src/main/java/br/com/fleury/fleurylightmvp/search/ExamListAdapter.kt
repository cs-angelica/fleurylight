package br.com.fleury.fleurylightmvp.search

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import br.com.fleury.fleurylightmvp.R

class ExamListAdapter(private val activity: SearchExamActivity, private var examList: ArrayList<Exam>): RecyclerView.Adapter<ExamListAdapter.ViewHolder>(){

    var selectedItems = 0

    fun changeItems(filteredData: ArrayList<Exam>) {
        examList = filteredData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        val parentView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_exam_search, parent, false)

        return ViewHolder(parentView,
                parentView.findViewById(R.id.checkExam),
                parentView.findViewById(R.id.examName),
                parentView.findViewById(R.id.examAvailable))
    }

    override fun getItemCount() = examList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.examName.text = examList[position].name

        holder.checkExam.isEnabled = examList[position].mobileAvailable
        if(examList[position].mobileAvailable) {
            holder.examAvailable.setText(R.string.mobile_available)
        } else {
            holder.examAvailable.setText(R.string.mobile_not_available)
        }
        holder.checkExam.isChecked = examList[position].isSelected

        holder.checkExam.setOnClickListener {
            val isChecked = holder.checkExam.isChecked

            examList[position].isSelected = isChecked

            if(isChecked){
                selectedItems++
            } else {
                selectedItems--
            }

            if(selectedItems < 0) {
                selectedItems = 0
            }
            else if(selectedItems > examList.size) {
                selectedItems = examList.size
            }

            activity.updateSelectedItemsLabel(selectedItems)
        }
    }

    class ViewHolder(parentView: View,
                     val checkExam: CheckBox,
                     val examName: TextView,
                     val examAvailable: TextView) : RecyclerView.ViewHolder(parentView)


}