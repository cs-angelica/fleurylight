package br.com.fleury.fleurylightmvp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import br.com.fleury.fleurylightmvp.search.SearchExamActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_schedule.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonSchedule.setOnClickListener {
            startActivity(Intent(this, SearchExamActivity::class.java))
        }

        val examName = getSavedExamName()
        examName?.let {
            examEmpty.visibility = View.GONE
            nextExam.visibility = View.VISIBLE

            textExamName.text = it

            val examDate = getSavedExamDate()
            textScheduledStatus.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    R.drawable.ic_calendar, 0, 0, 0)
            textScheduledStatus.text = examDate ?: ""

            buttonDetail.text = getString(R.string.details)
            buttonDetail.setOnClickListener {
                startActivity(MyScheduleActivity.newIntent(this,
                        textExamName.text.toString(),
                        textScheduledStatus.text.toString()))
            }
        }


    }

    private fun getSavedExamName() : String?{
        val sharedPref = applicationContext.getSharedPreferences("br.com.fleury.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE)
        return sharedPref?.getString(getString(R.string.exam_name_pref), null)
    }

    private fun getSavedExamDate() : String?{
        val sharedPref = applicationContext.getSharedPreferences("br.com.fleury.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE)
        return sharedPref?.getString(getString(R.string.exam_date_pref), null)
    }
}